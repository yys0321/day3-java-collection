package com.thoughtworks.collection;

import java.util.List;
import java.util.stream.Collectors;

public class StreamReduce {

    public int getLastOdd(List<Integer> numbers) {
        return numbers.stream()
                .reduce(0, (a, b) -> b% 2 == 0 ? a : b);
        }

    public String getLongest(List<String> words) {
        return words.stream()
                .reduce("", (a, b) -> a.length() > b.length() ? a : b);
    }

    public int getTotalLength(List<String> words) {
        return words.stream()
                .reduce(0, (a, b) -> a + b.length(), Integer:: sum);
    }
}
